# Created by Sergio Constantino at 2019-01-25
# config.py
import logging
from json import load as load_json
from contextlib import contextmanager
from pathlib import Path


@contextmanager
def opened(filename, mode='r'):
    try:
        file = open(filename, mode)
    except IOError as err:
        yield None, err
    else:
        try:
            yield file, None
        finally:
            file.close()


class Config:
    def __init__(self, filename):
        with opened(filename) as (file, err):
            if err:
                logging.error(err)
            else:
                config = load_json(file)
                self.CONFIG_LOCATION = Path(filename)
                self.BROWSER_LOCATION = Path(config['BROWSER_LOCATION'])
                self.WEBDRIVER_LOCATION = Path(config['WEBDRIVER_LOCATION'])
                self.URL = str(config['URL'])
                logging.info(self)

    def __repr__(self):
        return "Config location: " + str(self.CONFIG_LOCATION.resolve()) + "\n"\
               + "Browser location: " + str(self.BROWSER_LOCATION.resolve()) + "\n"\
               + "Webdriver location: " + str(self.WEBDRIVER_LOCATION.resolve()) + "\n"\
               + "URL: " + self.URL


_BASE_DIR = Path.home() / 'PycharmProjects' / 'regcess'
ScrapperConfig = Config(_BASE_DIR / 'config' / 'config.json')
