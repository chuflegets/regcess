# Created by Sergio Constantino at 2019-01-30
# scrapper.py
from .config import ScrapperConfig
from selenium import webdriver
from selenium.webdriver.chrome.options import Options


def _get_webdriver_options():
    options = Options()
    options.binary_location = str(ScrapperConfig.BROWSER_LOCATION.resolve())
    options.headless = True
    return options


class Scrapper:
    _shared_state = {
        'wait': 10,
        'url': ScrapperConfig.URL,
        'browser': webdriver.Chrome(executable_path=str(ScrapperConfig.WEBDRIVER_LOCATION.resolve()),
                                    options=_get_webdriver_options())
    }

    def __init__(self, wait=2, url=''):
        self.__dict__ = self._shared_state
        if self.wait is None:
            self.wait = wait
        if self.url is None:
            self.url = url
        self.browser = self._shared_state['browser']

    def get(self, url):
        self.browser.get(url)


class RegcessScrapper(Scrapper):
    def __init__(self):
        super().__init__()
