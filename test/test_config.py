# Created by Sergio Constantino at 2019-01-28
# test_config.py
from unittest import TestCase
from regcess import Config
from regcess import CONFIG_FILE


class TestConfig(TestCase):
    def setUp(self):
        self.config = Config(CONFIG_FILE.resolve())
