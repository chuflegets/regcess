# Created by Sergio Constantino at 2019-01-23
# regcess.py
import logging
from scrapper import RegcessScrapper

logging.basicConfig(level=logging.INFO)

# center_selector = Select(driver.find_element_by_xpath("//select[@id='tipoCentroId']"))
# center_selector = Select(driver.find_element_by_id('tipoCentroId'))
# center_selector.select_by_value('E1')
# driver.find_element_by_class_name('formButon').click()

if __name__ == '__main__':
    scrapper = RegcessScrapper()
    scrapper.get(scrapper.url)
    scrapper.browser.implicitly_wait(scrapper.wait)
    select_fields = scrapper.browser.find_element_by_xpath('//label[text()="Comunidad Autónoma:"]/following-sibling::select')
